import React, { useState } from 'react'


export default function DogForm({onFormSubmit}) {
    const [dog, setDog] = useState({
        id: null,
        name:'',
        breed:'',
        age: 0
    });

    const handleChange = event => {
        setDog({
            ...dog,
            [event.target.name]: event.target.value
        });
    }

    const handleSubmit = event => {
        event.preventDefault();
        onFormSubmit(dog);
    }

    return (
        <form onSubmit={handleSubmit}>
            <label>Name :</label>
            <input type="text" name="name" onChange={handleChange} value={dog.name} />
            
            <label>Breed :</label>
            <input type="text" name="breed" onChange={handleChange} value={dog.breed} />
            
            <label>Age :</label>
            <input type="number" name="age" onChange={handleChange} value={dog.age} />

            <button>Add</button>
        </form>
    );
}