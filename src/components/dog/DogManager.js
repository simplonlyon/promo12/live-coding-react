import React, { useState, useEffect } from 'react'
import DogList from './DogList'
import DogForm from './DogForm';
import { fetchDog, postDog } from '../../service/dog-service';


export default function DogManager() {
    const [dogs, setDogs] = useState([]);

    useEffect(() => {
        fetchDog().then(dogs => setDogs(dogs));
    }, []);

    const handleDelete = (dog) => {
        setDogs(dogs.filter(item => item.id !== dog.id));
    }

    const handleForm = (dog) => {
        postDog(dog).then(data => {
            setDogs([
                ...dogs,
                data
            ]);
        });
    }

    return (
        <div>
            <DogList dogs={dogs} onDogClick={handleDelete}  />
            <DogForm onFormSubmit={handleForm} />
        </div>
    )
}