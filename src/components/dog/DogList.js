import React from 'react'



export default function DogList({dogs, onDogClick}) {

    return (
        <ul>
            {dogs.map(dog => <li key={dog.id}>{dog.name} <button onClick={() => onDogClick(dog)}>X</button></li>)}

        </ul>
    )
}