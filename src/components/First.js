import React, { useState } from 'react';


export default function First() {
    const [message, setMessage] = useState('mon message');
    

    return (
        <div onClick={() => setMessage('autre chose')}>{message}</div>
    );
}