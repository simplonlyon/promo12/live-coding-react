import React from 'react';

import './App.css';
import First from './components/First';
import DumbComp from './components/DumbComp';
import DogManager from './components/dog/DogManager';


function App() {
  return (
    <div className="App">
      <DogManager />

      <First />
      <DumbComp text="test" />
    </div>
  );
}

export default App;
