

const apiUrl = 'http://localhost:4000/dog';


export async function fetchDog() {
    const response = await fetch(apiUrl);
    const data = await response.json();
    return data;
}


export async function postDog(dog) {

    const response = await fetch(apiUrl, {
        method: 'POST',
        body: JSON.stringify({...dog, birthdate: '2020-01-02'}),//Un peu naze, mais c'est juste pour aller avec mon api rest qu'est pas ouf ^^
        headers: {
            'Content-Type':'application/json'
        }
    });
    const data = await response.json();
    return data;
}